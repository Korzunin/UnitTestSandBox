﻿using FakeItEasy;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoFakeItEasy;
using Ploeh.AutoFixture.NUnit3;
using UnitTestSandBox.Logic;

namespace UnitTestSandBox
{
    class AutoFixtureTests
    {
        [Test, AutoFakeItEasyData]
        public void TestAutoFixture(/*Email message, [Frozen] Fake<IReplacer> replacer, Smtp sender*/)
        {
            // Arrange
            var fixture = new Fixture().Customize(new AutoFakeItEasyCustomization());
            var message = fixture.Build<Email>().With(email => email.To, "TestTo").Create();
            var replacer = fixture.Freeze<Fake<IReplacer>>();
            replacer.CallsTo(x => x.Replace(message.Body)).Returns("Hello, Friend!");
            var sender = fixture.Create<Smtp>();

            // Act
            var result = sender.Send(message);

            // Assert
            Assert.AreEqual(result, "Message 'Hello, Friend!' has been sent to 'TestTo'", "Wrong message");
        }
    }

    public class AutoFakeItEasyDataAttribute : AutoDataAttribute
    {
        public AutoFakeItEasyDataAttribute()
            : base(new Fixture().Customize(new AutoFakeItEasyCustomization()))
        {
        }
    }
}
