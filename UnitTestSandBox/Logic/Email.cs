﻿using System;

namespace UnitTestSandBox.Logic
{
    public interface IMessage
    {
        Guid Id { get; }
        string To { get; set; }
        string Body { get; set; }
    }

    public class Email : IMessage
    {
        public Guid Id { get; set; }
        public string To { get; set; }
        public string Body { get; set; }

        public Email()
        {
            Id = Guid.NewGuid();
        }

        public Email(string to, string body) : this()
        {
            To = to;
            Body = body;
        }
    }
}