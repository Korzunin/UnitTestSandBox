namespace UnitTestSandBox.Logic
{
    public interface IReplacer
    {
        string Replace(string input);
    }

    public class Replacer : IReplacer
    {
        public string Replace(string input)
        {
            return input.Replace("{pattern}", "Some Text");
        }
    }
}