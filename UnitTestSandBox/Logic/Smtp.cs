namespace UnitTestSandBox.Logic
{
    public interface ISender
    {
        string Send(IMessage message);

        IMessage GetLastSentMessage();
    }

    public class Smtp : ISender
    {
        private readonly IReplacer replacer;

        public Smtp(IReplacer replacer)
        {
            this.replacer = replacer;
        }

        public string Send(IMessage message)
        {
            var body = replacer.Replace(message.Body);
            // ... difficult logic
            return $"Message '{body}' has been sent to '{message.To}'";
        }

        public IMessage GetLastSentMessage()
        {
            // ... complicated logic
            return new Email("Someone", "Something");
        }
    }
}