﻿using FakeItEasy;
using Moq;
using NUnit.Framework;
using UnitTestSandBox.Logic;

namespace UnitTestSandBox
{
    public class FakeItEasyTests
    {
        [Test]
        public void TestMoq()
        {
            // Arrange
            var message = new Email("Friend", "Hello, {pattern}!");
            var replacer = new Mock<IReplacer>(MockBehavior.Strict);
            replacer.Setup(r => r.Replace(message.Body)).Returns("Hello, Friend!");
            var sender = new Smtp(replacer.Object);

            // Act
            var result = sender.Send(message);

            // Assert
            Assert.AreEqual(result, "Message 'Hello, Friend!' has been sent to 'Friend'", "Wrong message");
        }

        [Test]
        public void TestFake()
        {
            // Arrange
            var message = new Email("Friend", "Hello, {pattern}!");
            var replacer = new Fake<IReplacer>(o => o.Strict());
            replacer.CallsTo(r => r.Replace(message.Body)).Returns("Hello, Friend!");
            var sender = new Smtp(replacer.FakedObject);

            // Act
            var result = sender.Send(message);

            // Assert
            Assert.AreEqual(result, "Message 'Hello, Friend!' has been sent to 'Friend'", "Wrong message");
        }
    }
}
