﻿using FakeItEasy;
using FluentAssertions;
using NUnit.Framework;
using UnitTestSandBox.Logic;

namespace UnitTestSandBox
{
    public class FluentAssertionsTests
    {
        [Test]
        public void TestFluent()
        {
            // Arrange
            var replacer = A.Fake<IReplacer>(o => o.Strict());
            var sender = new Smtp(replacer);

            // Act
            var result = sender.GetLastSentMessage();

            // Assert
            var expectation = new Email("Someone", "Something");
            result.ShouldBeEquivalentTo(expectation);
        }
    }
}
