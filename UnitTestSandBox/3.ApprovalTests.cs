﻿using System.CodeDom.Compiler;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using ApprovalTests;
using ApprovalTests.Reporters;
using FakeItEasy;
using Newtonsoft.Json;
using NUnit.Framework;
using StatePrinting;
using StatePrinting.OutputFormatters;
using UnitTestSandBox.Logic;

namespace UnitTestSandBox
{
    public class ApprovalTests
    {
        [Test]
        [UseReporter(typeof(DiffReporter))]
        public void TestApproval()
        {
            // Arrange
            var replacer = A.Fake<IReplacer>(o => o.Strict());
            var sender = new Smtp(replacer);

            // Act
            var result = sender.GetLastSentMessage();

            // Assert
            var json = JsonConvert.SerializeObject(result);
            Approvals.Verify(json);
        }
    }
}
